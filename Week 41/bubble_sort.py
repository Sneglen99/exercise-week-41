def bubble_sort(data):
    dataList = list(data)
    loops = len(dataList) - 1
    while loops > 0:
        for idx in range(loops):
            first = dataList[idx]
            second = dataList[idx+1]
            if first > second:
                dataList[idx] = second
                dataList[idx+1] = first
        loops -= 1
    return dataList
            


if __name__ == "__main__":
    for data in ((),
                 (1,),
                 (1, 3, 8, 12),
                 (12, 8, 3, 1),
                 (8, 3, 12, 1)):
        print('{!s:>15} --> {!s:>15}'.format(data, bubble_sort(data)))
