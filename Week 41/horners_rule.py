import pandas as pd

def p(a, x):
    a.reverse()
    result = 0
    for a_n in a[:-1]:
        result += a_n
        result *= x
    result += a[-1]
    return result

if __name__ == "__main__":
    expected = [4, 5, 6, 4.75, 1027]
    lists = [[4], [5, -2, 3], [5, -2, 3], [5, -2, 3], [3,0,0,0,0,0,0,0,0,0,1]]
    xs = [0, 0, 1, 0.5, 2]
    polynoms = []

    computed = []
    for list, x in zip(lists,xs):
        computed.append(p(list,x))
        list.reverse()
        polynom = ''
        for idx, a_n in enumerate(list):
            if a_n > 0 and idx != 0:
                polynom += '+'
            if a_n != 0:
                if idx > 1:
                    polynom += f'{a_n}x^{idx}'
                elif idx == 1:
                    polynom += f'{a_n}x'
                else:
                    polynom += f'{a_n}'
        polynoms.append(polynom)

    data = pd.DataFrame()
    data['Polynomial'] = polynoms
    data['x'] = xs
    data['Expected'] = expected
    data['Computed'] = computed
    print(data)
