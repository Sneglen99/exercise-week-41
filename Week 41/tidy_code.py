from random import randint as randomInteger

__author__ = 'Stein Oskar Hinderaker'
__email__ = 'stein.oskar.hinderaker@nmbu.no'


def userGuess():
    userInput = 0
    while userInput < 1:
        userInput = int(input('Your guess: '))
    return userInput


def rollDice():
    return randomInteger(1, 6) + randomInteger(1, 6)


def equalCheck(x1, x2):
    return x1 == x2


if __name__ == '__main__':

    guess = False
    tries = 3
    dice = rollDice()
    while not guess and tries > 0:
        guess = userGuess()
        guess = equalCheck(dice, guess)
        if not guess:
            print('Wrong, try again!')
            tries -= 1

    if tries > 0:
        print('You won {} points.'.format(tries))
    else:
        print('You lost. Correct answer: {}.'.format(dice))
