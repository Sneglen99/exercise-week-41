def letter_freq(txt):
    letters = {}
    for letter in txt.lower():
        if letter in letters.keys():
           letters[letter] += 1
        else:
            letters[letter] = 1
    return letters
    


if __name__ == '__main__':
    text = input('Please enter text to analyse: ')

    frequencies = letter_freq(text)
    for letter, count in sorted(frequencies.items()):
        print('{:3}{:10}'.format(letter, count))
